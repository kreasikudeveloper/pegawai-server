package com.balicamp.training.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.balicamp.training.demo.model.Pegawai;

@RestController
@CrossOrigin
public class PegawaiController {
	
	@GetMapping("/haloo")
	public String haloo() {
		return "Testing";
	}
	
	@GetMapping("/pegawai")
	public List<Pegawai> getPegawai(){
		List<Pegawai> pegawais = new ArrayList<>();
		
		Pegawai peg = new Pegawai(
				"013232","Balicamp","Pacung Baturiti Test","https://cdn2.iconfinder.com/data/icons/user-management/512/link-512.png");
		Pegawai peg2 = new Pegawai(
				"013233","Telkomsigma","BSD Jakarta","https://cdn2.iconfinder.com/data/icons/user-management/512/link-512.png");

		pegawais.add(peg);
		pegawais.add(peg2);
		
		return pegawais;
	}
	

}
