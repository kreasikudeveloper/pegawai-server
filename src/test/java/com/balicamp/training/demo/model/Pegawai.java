package com.balicamp.training.demo.model;

public class Pegawai {

	 	public String nik;
		public String nama;
		public String alamat;
		public String pathfoto;
		
		public Pegawai() {
			// TODO Auto-generated constructor stub
		}

		public Pegawai(String nik, String nama, String alamat, String pathfoto) {
			super();
			this.nik = nik;
			this.nama = nama;
			this.alamat = alamat;
			this.pathfoto = pathfoto;
		}

		public String getNik() {
			return nik;
		}

		public void setNik(String nik) {
			this.nik = nik;
		}

		public String getNama() {
			return nama;
		}

		public void setNama(String nama) {
			this.nama = nama;
		}

		public String getAlamat() {
			return alamat;
		}

		public void setAlamat(String alamat) {
			this.alamat = alamat;
		}

		public String getPathfoto() {
			return pathfoto;
		}

		public void setPathfoto(String pathfoto) {
			this.pathfoto = pathfoto;
		}
		
		
		
		
}
